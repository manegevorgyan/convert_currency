<?php

namespace App\Console\Commands;

use App\Models\Currency;
use Illuminate\Console\Command;
use SimpleXMLElement;

class GetCurrency extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:currency';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get currency and send to db';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', 'http://www.cbr.ru/scripts/XML_daily.asp', [
            'headers' => [
                'Content-type' => "application/xml",
                'Accept' => ['application/xml', 'charset' => 'utf-8'],
                ],
            'timeout' => 120,
        ])->getBody()->getContents();
        $response = new SimpleXMLElement($response);
        foreach($response as $currency) {
            $this->addToCurrencyTable($currency);
        };
        return "currencies were added";
    }
    private function addToCurrencyTable($currency)
    {
        $currencyModel = Currency::query()->where('valute_id', $currency->ID)->first();
        if($currencyModel != null) {
            $currencyModel->update(['value' => $currency->Value]);
        } else {
            $model = new Currency;
            $model->valute_id = $currency["ID"];
            $model->num_code = $currency->NumCode;
            $model->char_code = $currency->CharCode;
            $model->nominal = $currency->Nominal;
            $model->name = $currency->Name;
            $model->value = (float)str_replace(',', '.', $currency->Value);
            $model->save();
        }
    }
}
