<?php

namespace App\Http\Controllers;

use App\Models\Currency;
use Illuminate\Http\Request;

class CurrencyController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function currency()
    {
        return response()->json(Currency::all(), 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function convert(Request $request)
    {
        $fromCurrency = Currency::query()->where('char_code', $request->post('fromCode'))->first();
        $toCurrency = Currency::query()->where('char_code', $request->post('toCode'))->first();
        $value = $request->post('value');

        if( !$fromCurrency || !$toCurrency || !$value) return response()->json('send all fields', 500);

        return response()->json($value * ($fromCurrency->value /$fromCurrency->nominal) / ($toCurrency->value / $toCurrency->nominal), 200);
    }
}
