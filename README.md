<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

## Steps to Run Project
- clone project
- run dependencies with composer install
- generate key
- copy .env and .env.testing
- run migrations
- run cron "php artisan get:currency" to get currency from api. 
    It will be updated every day (it is made for not to send so many requests)
- run tests with "vendor/bin/codecept run functional CurrencyCest"
- if i forgot something sorry for that...

