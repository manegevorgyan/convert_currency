<?php

class CurrencyCest
{
    /**
     * @param FunctionalTester $I
     */
    public function testCurrency(FunctionalTester $I)
    {
        $I->amOnAction(\App\Http\Controllers\CurrencyController::class . '@currency');
        $I->canSeeResponseCodeIs(200);
    }

    /**
     * @param FunctionalTester $I
     */
    public function testConvert(FunctionalTester $I)
    {
        $I->amOnAction(\App\Http\Controllers\CurrencyController::class . '@convert', [
            'fromCode' => 'AMD',
            'toCode' => 'USD',
            'value' => 5000
        ]);
        $I->canSeeResponseCodeIs(200);
    }
}
